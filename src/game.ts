import ioHook from 'iohook'
import Character from './character'
import Grid from './Grid'

const hrtimeMs = function() {
	let time = process.hrtime()
	return time[0] * 1000 + time[1] / 1000000
}

class Game {
	FPS = 20
	tick = 0
	previous = hrtimeMs()
	tickLengthMs = 1000 / this.FPS

	position = 0

	interval: NodeJS.Timeout

	character = new Character()

	init() {
		// process.stdout.on('resize', () => {
		// 	// console.log('screen size has changed!')
		// 	// console.log(`${process.stdout.columns}x${process.stdout.rows}`)
		// })
	}

	run() {
		setTimeout(() => this.run(), this.tickLengthMs)
		let now = hrtimeMs()
		let delta = (now - this.previous) / 1000
		// console.log('delta', delta)
		// const FPS = Math.round(1 / delta)
		// console.log('FPS : ', FPS)
		// game.update(delta, tick) // game logic would go here

		this.update(delta)
		this.render()
		this.previous = now
		this.tick++
	}

	update(delta: number) {
		this.character.update(delta)
	}

	render() {
		const grid = new Grid()
		console.clear()
		this.character.render(grid)
		grid.display()
	}

	end() {
		// clearInterval(this.interval)
	}
}

const g = new Game()
g.init()
g.run()

process.on('exit', () => {
	g.end()
	ioHook.unload()
})
