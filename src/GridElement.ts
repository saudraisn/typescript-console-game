import Grid from './Grid'

export interface GameElement {
	render(grid: Grid): void
	update(delta: number): void // delta is the amount of seconds that passed since last update.
}
