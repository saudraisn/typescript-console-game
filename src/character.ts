import Grid from './Grid'
import { GameElement } from './GridElement'
import keyboard from './Keyboard'

export class Character implements GameElement {
	lastRendered = Date.now()
	x = 0
	y = process.stdout.rows / 2 - 7

	isMoving = false

	speed = 30

	moveTime = 0
	moveAnimDelay = 0.2

	template1 = `
 XXXX XX
XX   X XX
XX  XXX XX
 XXXXXXXX
    X     XXX    XXX
     X   XXX XXXX
     XXXX XX
     XXX
     XX XXX
     XX   X
     X
    XX
  XXX XX
 XX     X
X        X
  `

	template2 = `
 XXXX XX
XX   X XX
XX  XXX XX
 XXXXXXXX
    X     XXX    XXX
     X   XXX XXXX
     XXXX XX
     XXX
     XX XXX
     XX   X
     X
    XX
    XXX
    X X
    X X
  `

	state = this.template1

	update(deltaT: number): void {
		if (keyboard.d) {
			this.x += this.speed * deltaT
			this.isMoving = true
		}

		if (keyboard.a) {
			this.x -= this.speed * deltaT
			this.isMoving = true
		}

		if (!keyboard.d && !keyboard.a) {
			this.isMoving = false
		}

		if (this.isMoving) {
			this.moveTime += deltaT
			if (this.moveTime > this.moveAnimDelay) {
				this.switchTemplate()
				this.moveTime = this.moveTime % this.moveAnimDelay
			}
		} else {
			this.state = this.template1
		}
	}

	switchTemplate() {
		if (this.state === this.template1) {
			this.state = this.template2
		} else {
			this.state = this.template1
		}
	}

	render(grid: Grid): void {
		const rows = this.state.split('\n')

		rows.forEach((row, index) => {
			for (var i = 0; i < row.length; i++) {
				const rowIndex = index + Math.round(this.y)
				const colIndex = i + Math.round(this.x)

				grid.grid[rowIndex][colIndex] = row.charAt(i)
			}
		})
	}
}

export default Character
