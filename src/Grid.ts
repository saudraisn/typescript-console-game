export class Grid {
	public grid: string[][]

	constructor(public nbCols: number = process.stdout.columns, public nbRows: number = process.stdout.rows) {
		this.grid = new Array(nbRows).fill(' ').map(() => new Array(nbCols).fill(' '))
	}

	display() {
		console.log(this.grid.map((row) => row.join('')).join(''))
	}
}

export default Grid
