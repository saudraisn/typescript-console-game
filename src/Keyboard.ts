import ioHook from 'iohook'

class Keyboard {
	a = false
	d = false
	space = false

	constructor() {
		ioHook.on('keydown', (e) => {
			if (e.keycode === 32) {
				// d
				this.d = true
			}
			if (e.keycode === 30) {
				// a
				this.a = true
			}
			if (e.keycode === 57) {
				// space
				this.space = true
			}
		})

		ioHook.on('keyup', (e) => {
			if (e.keycode === 32) {
				// d
				this.d = false
			}
			if (e.keycode === 30) {
				this.a = false
			}
			if (e.keycode === 57) {
				this.space = false
			}
		})

		ioHook.start()
	}
}

export const keyboard = new Keyboard()
export default keyboard
